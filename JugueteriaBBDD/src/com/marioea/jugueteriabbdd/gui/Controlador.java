package com.marioea.jugueteriabbdd.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class Controlador implements ActionListener, TableModelListener {
    /**
     * @author Mario Escos
     * @version 1.8
     */
    private Vista vista;
    private Modelo modelo;
//creamos una enumeracion en la cual añadimos la palbra conectado y desconectado
    private enum tipoEstado {conectado, desconectado};

    private tipoEstado estado;
//constructor de controlador

    /**
     *
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;
//iniciamos las tablas creadas para que de esta manera se añadan
// los encabezados de cada  tabla
        iniciarTabla();
        iniciarTabla1();
        iniciarTabla2();
        iniciarTabla3();
        iniciarTabla4();
        //le damos accion al action listener
        addActionListener(this);
        //le damos accion al modelo de tabla
        addTableModelListeners(this);
    }


    /**
     * le damos funcionalidad a cada boton que hemos creado anteriormente
     *  tambien le damos funcionalidad a los botoncitos del desplegable de arriba a la izquierda
     *   a la hora de ejecutar el programa
     * @param listener
     */
    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnMarcaTipo.addActionListener(listener);
        vista.btnMarcaTipoTotal.addActionListener(listener);
        vista.btnTodosDatosPorRango.addActionListener(listener);

        vista.juguetesPorTipoButton.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    /**
     * le damos funcionalidad a todas las tablas que hemos creado para cada sentencia de sql
     * @param listener
     */

    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
        vista.dtm2.addTableModelListener(listener);
        vista.dtm3.addTableModelListener(listener);
        vista.dtm4.addTableModelListener(listener);

    }


    /**
     * este metodo se encarga de actualizar la tabla cuando queremos modificar un elemento
     * si hemos decidido actualizar la tabla lo notifica mostrando un mensaje de actualizada
     * en consola y tambien cambia el label que hemos creado y muestra que la columna esta actualizada
     * ademas de cambiar la palabra que deseamos cambiar
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarJuguete(Integer.parseInt(String.valueOf((Integer)vista.dtm.getValueAt(filaModicada,0))),String.valueOf((String)vista.dtm.getValueAt(filaModicada,1)),String.valueOf((String)vista.dtm.getValueAt(filaModicada,2)),Double.parseDouble(String.valueOf((Double) vista.dtm.getValueAt(filaModicada,3))),String.valueOf((String)vista.dtm.getValueAt(filaModicada,4)),String.valueOf((String)vista.dtm.getValueAt(filaModicada,5)), Timestamp.valueOf(String.valueOf(vista.dtm.getValueAt(filaModicada,6))));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * aqui he creado un switch para añadirle la funcion
     * que van a desarrollar en cada caso
     * cada boton
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Juguetes por Tipo":
                try {
                    //coge el metodo de mostrar los juguetes y cuantos de ese tipo hay
                    modelo.JuguetesPorTipo();
                    //carga las filas y obtiene los datos para de esa manera mostrarlo en la tabla
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }


                break;
            case "Marca Tipo Total Por Precio y Marca":
                try {
                    //coge el metodo de mostrar la marca y el tipo por precio menor o igual a 17
                    //y la marca que corresponde a playmobil
                    modelo.MarcaTipoTotalPorPrecioyMarca();
                    //carga las filas y obtiene los datos para mostrarlos en la tabla
                    cargarFilas4(modelo.obtenerDatos4());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Todos Datos por Rango de Precio":
                try {
                    //muestra todos los datos cuyo precio es mayor de 17
                    modelo.TodosDatosPorRangoPrecioMayor();
                    //carga las filas y obtiene los datos para mostrarlos en la tabla
                    cargarFilas3(modelo.obtenerDatos3());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Marca y Tipo por CalidadBuena":
                try {
                    //muestra la marca y el tipo de juguete siempre que sea de buena calidad
                    modelo.MarcaTipoPorCalidadBuena();
                    //carga las filas y obtiene los datos para mostrarlos en la tabla
                    cargarFilas2(modelo.obtenerDatos2());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Crear tabla Juguete":
                try {
                    //coge el metodo de modelo para crear la tabla de juguete
                    modelo.crearTablaJuguete();
                    //en el label muestra que la tabla esta creada
                    vista.lblAccion.setText("Tabla juguete creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Nuevo":
                try {
                    //rellenamos con este metodo todos los txt del programa
                    //una vez añadidos se da de alta automaticamente un juguete que hemos creado

                    modelo.insertarJuguete(vista.txtMarca.getText(),vista.txtTipo.getText(),Double.valueOf(vista.txtRango.getText()),vista.txtSeccion.getText(),vista.txtCalidad.getText(),vista.DateTimePicker.getDateTimePermissive());
                    //carga las filas y obtiene los datos para mostrarlos en la tabla
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                //creamos un string de marca llamamos al txt de buscar y cogemos el texto que
                //hemos escrito
                String marca=vista.txtBuscar.getText();
                try {
                    //llamamos al result set que se encarga de almacenar la consulta
                    //busca automaticamente el resultado mediante el metodo buscar
                    ResultSet rs =modelo.buscarJuguete(marca);
                    //aqui carga las filas y restablece la tabla principal
                    // mostrando unicamente el resultado que hemos deseado buscar
                    cargarFilas(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                try {
                    //en este boton seleccionamos la fila a borrar y le damos a borrar
                    //entonces lo elimina por completo de la tabla
                    int filaBorrar=vista.tabla.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarJuguete(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;



            case "Salir":
                //unicamente salimos del programa
                System.exit(0);
                break;

            case "Conectar":
                //creamos un if y un else para decir cuando estamos conectados y si es asi que
                //muestre los datos en la tabla principal
                //sino que nos avise si estamos desconectados y de esta manera no nos
                //dejara realizar ninguna operacion a realizar
                if (estado==tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }


                }
                break;
        }
    }

    /**
     * creamos el metodo que contendra todos las cabeceras y las añadimos a la tabla
     */

    private void iniciarTabla(){
        String[] headers={"id","marcaJuguete","tipoJuguete","rangoPrecio","seccion","calidadJuguete","fecha fabricacion"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 7
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

    /**
     * creamos el metodo que contendra todos las cabeceras y las añadimos a la tabla
     */

    private void iniciarTabla1(){
        String[] headers={"Tipo","Total"};
        vista.dtm1.setColumnIdentifiers(headers);
    }

    /**
     * creamos el metodo que contiene el numero de fila con las que contara la tabla en este caso 2
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

    /**
     * creamos el metodo que contendra todos las cabeceras y las añadimos a la tabla
     */

    private void iniciarTabla2(){
        String[] headers={"Tipo","Marca"};
        vista.dtm2.setColumnIdentifiers(headers);
    }

    /**
     * creamos el metodo que contiene el numero de fila con las que contara la tabla en este caso 2
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilas2(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm2.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm2.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

    /**
     * creamos el metodo que contendra todos las cabeceras y las añadimos a la tabla
     */

    private void iniciarTabla3(){
        String[] headers={"id","marcaJuguete","tipoJuguete","rangoPrecio","seccion","calidadJuguete","fecha fabricacion"};
        vista.dtm3.setColumnIdentifiers(headers);
    }

    /**
     * creamos el metodo que contiene el numero de fila con las que contara la tabla en este caso 7
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilas3(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtm3.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm3.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

    /**
     * creamos el metodo que contendra todos las cabeceras y las añadimos a la tabla
     */

    private void iniciarTabla4(){
        String[] headers={"Marca","Tipo","Total"};
        vista.dtm4.setColumnIdentifiers(headers);
    }

    /**
     * creamos el metodo que contiene el numero de fila con las que contara la tabla en este caso 3
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilas4(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[3];
        vista.dtm4.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);

            vista.dtm4.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }





}
