package com.marioea.jugueteriabbdd.gui;

public class Principal {
    public static void main(String args[]) {
        //Principal para ejecutar la aplicacion
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
