package com.marioea.jugueteriabbdd.gui;

import java.sql.*;
import java.time.LocalDateTime;

public class Modelo {
    /**
     * @author Mario Escos
     * @version 1.8
     */
    //declaramos la variable conexion para acceder asi a la bbdd
    private Connection conexion;

    /**
     * metodo que crea la tabla de juguete coge la url
     * el user y contraseña
     * llama al procedimiento
     * y ejecuta la sentencia
     * @throws SQLException
     */

    public void crearTablaJuguete() throws SQLException {
        conexion=null;
        //escribimos la url de nuestra bbdd nuestro user y nuestra clave
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/jugueteria","root","");
        //llamamos al procedimiento que crea la tabla de juguete
        String sentenciaSql="call crearTablaJuguete()";
        CallableStatement procedimiento=null;
        //prepara la sentencia y la ejecuta
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * metodo para conectar la bbdd con nuestro programa
     * @throws SQLException
     */

    public void conectar() throws SQLException {
        conexion=null;
        //conectamos mediante la url de nuestra bbdd nuestro user y nuestra clave
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/jugueteria","root","");

    }

    /**
     * metodo para desconectar la bbdd con nuestro programa
     * @throws SQLException
     */

    public void desconectar() throws SQLException {
        //cerramos la conexion
        conexion.close();
        conexion=null;
    }


    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * principal
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger todos los datos de juguete
        String consulta ="SELECT * FROM juguete";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * segunda
     * @return
     * @throws SQLException
     */

    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger el tipo de juguete y el numero de los que hay
        //agrupados por el tipo de juguete
        String consulta ="SELECT tipoJuguete,COUNT(*) FROM juguete GROUP BY tipoJuguete";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * tercera
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos2() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger el tipo de juguete y la marca
        //cuando la calidad es buena
        String consulta ="SELECT tipoJuguete, marcaJuguete FROM juguete WHERE  calidadJuguete='Buena'";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * cuarta
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos3() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger todos los datos
        //cuando el rango de precio es mayor que 17
        String consulta =" SELECT * FROM juguete WHERE  rangoPrecio>17";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * quinta
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos4() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger la marca y el tipo de juguete y el total
        //cuando el rango de precio es menor o igual que 17 y la marca es playmobil
        String consulta ="SELECT marcaJuguete,tipoJuguete, COUNT(*)  FROM juguete WHERE marcaJuguete='PLAYMOBIL' OR  rangoPrecio<=17";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     *  metodo que sirve para insertar los datos en la tabla principal creamos una variable
     *  para cada dato a introducir
     * @param marcaJuguete
     * @param tipoJuguete
     * @param rangoPrecio
     * @param seccion
     * @param calidadJuguete
     * @param fechaFabricacion
     * @return
     * @throws SQLException
     */
    public int insertarJuguete(String marcaJuguete, String tipoJuguete, Double rangoPrecio, String seccion, String calidadJuguete, LocalDateTime fechaFabricacion) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;
    //realizamos una consulta que inserta en la tabla de juguete una marca, un tipo, rango de precio,
        //seccion, calidad y una fecha sustituyendolas por cada valor mostrado como una interrogacion
        String consulta="INSERT INTO juguete(marcaJuguete, tipoJuguete, rangoPrecio, seccion, calidadJuguete,fecha_fabricacion)"+
                "VALUES (?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        //muestramos los datos en el siguiente orden
        sentencia.setString(1,marcaJuguete);
        sentencia.setString(2,tipoJuguete);
        sentencia.setDouble(3,rangoPrecio);
        sentencia.setString(4,seccion);
        sentencia.setString(5,calidadJuguete);
        sentencia.setTimestamp(6,Timestamp.valueOf(fechaFabricacion));

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }

    /**
     * metodo para eliminar una fila
     * elimina una fila cuando la id sea el valor sea el de la fila seleccionada
     * @param id
     * @return
     * @throws SQLException
     */

    public int eliminarJuguete(int id) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="DELETE FROM juguete WHERE id=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;

    }

    /**
     * metodo para modificar un juguete en el cual unicamente
     * podemos modificar el dato que nosotros deseamos de la tabla
     * @param id
     * @param marcaJuguete
     * @param tipoJuguete
     * @param rangoPrecio
     * @param seccion
     * @param calidadJuguete
     * @param fechaFabricacion
     * @return
     * @throws SQLException
     */

    public int modificarJuguete (int id, String marcaJuguete, String tipoJuguete, Double rangoPrecio, String seccion, String calidadJuguete, Timestamp fechaFabricacion) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE juguete SET marcaJuguete=?, tipoJuguete=?,"+
                "rangoPrecio=?,seccion=?,calidadJuguete=?,fecha_fabricacion=? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,marcaJuguete);
        sentencia.setString(2,tipoJuguete);
        sentencia.setDouble(3,rangoPrecio);
        sentencia.setString(4,seccion);
        sentencia.setString(5,calidadJuguete);
        sentencia.setTimestamp(6,fechaFabricacion);
        sentencia.setInt(7,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;


    }

    /**
     * metodo para buscar un juguete por marca del juguete
     * @param marcaJuguete
     * @return
     * @throws SQLException
     */

    public ResultSet buscarJuguete(String marcaJuguete) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual cogemos mostramos todos los datos cuando la marca
        //del juguete sea igual a la introducida
        String consulta = "SELECT * FROM juguete WHERE marcaJuguete = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, String.valueOf(marcaJuguete));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        return resultado;





    }

    /**
     * metodo que recoge un script creado por nosotros el cual recoge un procedimiento
     * que muestra el tipo de juguete y el numero de ese juguete que hay
     * @throws SQLException
     */

    public void JuguetesPorTipo() throws SQLException {
        String sentenciaSql="call mostrarJugueteTipo()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }

    /**
     * metodo que recoge un script creado por nosotros el cual recoge un procedimiento
     * que muestra el tipo de juguete  y la marca  del  juguete simpre que la calidad sea buena
     * @throws SQLException
     */

    public void MarcaTipoPorCalidadBuena() throws SQLException {
        String sentenciaSql="call mostrarMarcaJugueteyTipoJugueteConCalidadBuena()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }

    /**
     * metodo que recoge un script creado por nosotros el cual recoge un procedimiento
     * que muestra todos los datos del  juguete siempre que el precio sea mayor de 17
     * @throws SQLException
     */

    public void TodosDatosPorRangoPrecioMayor() throws SQLException {
        String sentenciaSql="call mostrarTodosDatosporRangoPrecioMayor()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }

    /**
     * metodo que recoge un script creado por nosotros el cual recoge un procedimiento
     * que muestra marca, tipo y total del   juguete siempre que el precio sea menor o igual de 17 y la marca sea
     * igual a playmobil
     * @throws SQLException
     */

    public void MarcaTipoTotalPorPrecioyMarca() throws SQLException {
        String sentenciaSql="call mostrarMarcaTipoTotalPorPrecioyMarca()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }
}

