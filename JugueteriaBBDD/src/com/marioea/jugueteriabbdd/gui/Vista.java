package com.marioea.jugueteriabbdd.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
    JTextField txtMarca;
    JTextField txtTipo;
    JTextField txtRango;
    JTextField txtSeccion;
    JTextField txtCalidad;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JTextField txtBuscar;
    JLabel lblAccion;
    JTable tabla;
    JButton juguetesPorTipoButton;
    JTable tabla2;
    DateTimePicker DateTimePicker;
    JButton btnMarcaTipo;
    JTable tabla3;
    JButton btnTodosDatosPorRango;
    JTable tabla4;
    JButton btnMarcaTipoTotal;
    JTable tabla5;

//creamos todos los defaults table models
    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    DefaultTableModel dtm2;
    DefaultTableModel dtm3;
    DefaultTableModel dtm4;


    JMenuItem itemConectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;
    JFrame frame;

    public Vista() {
        frame = new JFrame("Jugueteria BBDD");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        tabla.setModel(dtm);

        dtm1 =new DefaultTableModel();
        tabla2.setModel(dtm1);
        dtm2 =new DefaultTableModel();
        tabla3.setModel(dtm2);
        dtm3 =new DefaultTableModel();
        tabla4.setModel(dtm3);
        dtm4 =new DefaultTableModel();
        tabla5.setModel(dtm4);



        crearMenu();

        frame.pack();
        frame.setVisible(true);

    }
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Juguete");
        itemCrearTabla.setActionCommand("CrearTablaJuguete");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}
