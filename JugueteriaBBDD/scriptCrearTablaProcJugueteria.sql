DELIMITER //
CREATE PROCEDURE crearTablaJuguete()
BEGIN
    CREATE TABLE juguete(
                           id int primary key auto_increment,
						   marcaJuguete varchar(30) unique,
							tipoJuguete varchar (40),
                            rangoPrecio varchar (40),
                            seccion varchar (40),
                            calidadJuguete varchar (40),
                            fecha_fabricacion timestamp);
END //
